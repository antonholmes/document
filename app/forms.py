from django import forms
from .models import Passport
from .models import Vy
from django.forms import ModelForm, TextInput, DateTimeInput, NumberInput

class PassportForm(ModelForm):
    class Meta:
        model = Passport
        fields = ['fam', 'name', 'otch', 'data_rozh', 'number_passport']

        widgets = {
            "fam": TextInput(attrs = {
                'class': 'form-control',
                'placeholder': 'Фамилия',
            }),
            "name": TextInput(attrs = {
                'class': 'form-control',
                'placeholder': 'Имя'
            }), 
            "otch": TextInput(attrs = {
                'class': 'form-control',
                'placeholder': 'Отчество'
            }),
            "data_rozh": DateTimeInput(attrs = {
                'class': 'form-control',
                'placeholder': 'Дата рождения'
            }),
            "number_passport": NumberInput(attrs = {
                'class': 'form-control',
                'placeholder': 'Серия и номер паспорта'
            }),
        }
class VyForm(ModelForm):
    class Meta:
        model = Vy
        fields = ['famvy', 'namevy', 'otchvy', 'data_rozhvy', 'number_vy']

        widgets = {
            "famvy": TextInput(attrs = {
                'class': 'form-control',
                'placeholder': 'Фамилия'
            }),
            "namevy": TextInput(attrs = {
                'class': 'form-control',
                'placeholder': 'Имя'
            }),
            "otchvy": TextInput(attrs = {
                'class': 'form-control',
                'placeholder': 'Отчество'
            }),
            "data_rozhvy": DateTimeInput(attrs = {
                'class': 'form-control',
                'placeholder': 'Дата рождения'
            }),
            "number_vy": NumberInput(attrs = {
                'class': 'form-control',
                'placeholder': 'Серия и номер водительского удостоверения'
            }),
        }
    