from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import TemplateView
from .forms import *
import base64
from django.core.files import File
from django.core.files.base import ContentFile
import cv2

from backend.cut import cutting
# Create your views here.

def index_page(request):
    request.session['return_path'] = request.path
    output = ' '
    error = ' '
    if request.method == 'POST':
        form = PassportForm(request.POST)
        if form.is_valid():
            form.save()
        if form.is_valid() == False:
            error ='Форма заполнена некоректно'
        else:
            error = 'Идет поиск информации'
            output = 'Гражданин находится в базе данных "РОЗЫСК"'
    
    form = PassportForm()
    data = {
           'form': form,
           'error': error,
           'output' : output
    }
    return render(request,"app/index1.html", data )

def Vy(request):
    request.session['return_path'] = request.path
    error = ' '
    if request.method == 'POST':
        form = VyForm(request.POST)
        if form.is_valid():
            form.save()
            # return redirect('') возврат на другую страницу
        else:
            error = 'Идет поиск информации'

    form = VyForm()
    data = {
           'form': form,
           'error': error
    }
    return render(request,"app/vy.html", data )



def face(request):
    request.session['return_path'] = request.path
    return render(request,"app/face.html" )

def Vy_face(request):
    return face(request)

def face_Vy(request):
    return Vy(request)

def document_page(request):
    return render(request, "app/document.html")


def backend(request):
    
    if request.POST:
        results = request.POST.get("testtext")                  #принимаем dataurl
        a = results.replace('data:image/png;base64,','')        #отрезаем лишнее(остается только код base64)   

        b = base64.b64decode(a)                                 #декодируем из base64 в бинарный

        with open('app/static/images/photo.jpg', 'wb') as f:    #открываем файл
                f.write(b)
        image = cv2.imread('app/static/images/test/test6.jpg')       #записываем в файл

        arrs = cutting(image)                                      #записываем в файл

        return redirect(request.session['return_path'])                            #перессылаем пользователя на страницу
    else:
        return render(request, 'app/back.html')                 #перессылаем пользователя на страницу

