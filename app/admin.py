from django.contrib import admin

# Register your models here.

from .models import Passport
admin.site.register(Passport)

from .models import Vy
admin.site.register(Vy)