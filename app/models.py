from django.db import models

# Create your models here.

class Passport(models.Model):
    fam = models.TextField('Фамилия', max_length=40)
    name = models.TextField('Имя', max_length=40)
    otch = models.TextField('Отчество', max_length=40)
    data_rozh = models.DateField('Дата рождения')
    number_passport = models.IntegerField('Серия и номер паспорта')

    def __str__(self):
        return self.fam
    
    class Meta:
        verbose_name = "Паспорт"
        verbose_name_plural = "Паспорт" # не знаю

class Canvas(models.Model):
    text = models.TextField('base64')

class Vy(models.Model):
    famvy = models.TextField('Фамилия', max_length=40)
    namevy = models.TextField('Имя', max_length=40)
    otchvy = models.TextField('Отчество', max_length=40)
    data_rozhvy = models.DateField('Дата рождения')
    number_vy = models.IntegerField('Серия и номер водительского удостоверения')

    def __str__(self):
        return self.famvy
    
    class Meta:
        verbose_name = "Водительское удостоверение"
        verbose_name_plural = "Водительское удостоверение" # не знаю