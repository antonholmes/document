from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
      path('backend/', views.backend),
      path('', views.index_page),
      path('document/', views.document_page),
      path('Vy/',views.Vy),
      path('face/',views.face),
       path('Vy/face/',views.face),
      path('face/Vy/',views.Vy),
      
]

urlpatterns += staticfiles_urlpatterns()