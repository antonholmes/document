# import the necessary packages
import os

# define the base path to the input dataset and then use it to derive
# the path to the images directory and annotation CSV file
BASE_PATH = "dataset"
IMAGES_PATH = os.path.sep.join([BASE_PATH, "photo2/"])
ANNOTS_PATH = os.path.sep.join([BASE_PATH, "obsh_s_n.csv"])


# define the path to the base output directory
BASE_OUTPUT = "backend/output"

# define the path to the output serialized model, model training plot,
# and testing image filenames
MODEL_PATH = os.path.sep.join([BASE_OUTPUT, "detector_s_n.h5"])
PLOT_PATH = os.path.sep.join([BASE_OUTPUT, "grafik_s_n.png"])
TEST_FILENAMES = os.path.sep.join([BASE_OUTPUT, "test_images.txt"])

# initialize our initial learning rate, number of epochs to train
# for, and the batch size
INIT_LR = 1e-4
NUM_EPOCHS = 35 #25	30
BATCH_SIZE = 128 #32 64 