from django.shortcuts import render
from django.http import HttpResponse
import base64
from django.core.files import File
from django.core.files.base import ContentFile

def backend(request):
    if request.POST:
        results = request.POST.get("testtext")              #принимаем dataurl
        a = results.replace('data:image/png;base64,','')    #отрезаем лишнее(остается только код base64)   

        b = base64.b64decode(a)                             #декодируем из base64 в бинарный

        with open('app/static/images/photo.jpg', 'wb') as f:                   #открываем файл
                f.write(b)                                  #записываем в файл

        return render(request, 'index1.html')            #перессылаем пользователя на страницу
    else:
        return render(request, 'back.html')            #перессылаем пользователя на страницу

