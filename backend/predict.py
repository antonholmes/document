# import the necessary packages
# from backend.pyimagesearch import config
# from backend.pyimagesearch import config_d_r
# from backend.pyimagesearch import config_lastname
# from backend.pyimagesearch import config_name
# from backend.pyimagesearch import config_fiOtch
# from backend.pyimagesearch import config_s_n
# from pyimagesearch import config
import tensorflow
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.models import load_model
# import numpy as np
import mimetypes
import argparse
import imutils
import cv2
import os
from PIL import Image


from backend.imagetotext import tesseract

def predant(arr2, number):

  imagePath = "app/static/images/scan/"

  if number == 1:
    # model = load_model(config.MODEL_PATH)
    name = "Output.jpg"
    startX = 97
    startY = 119
    endX = 779
    endY = 1043
  if number == 2:
    # model = load_model(config_s_n.MODEL_PATH)
    name = "SN.jpg"
    startX = 713
    startY = 238
    endX = 770
    endY = 487
    im = Image.open('app/static/images/scan/SN.jpg')
    im_rotate = im.rotate(90, expand=True)
    im_rotate.save('app/static/images/scan/SN.jpg', quality=95)
    im.close()
  if number == 3:
    # model = load_model(config_lastname.MODEL_PATH)
    name = "Last_name.jpg"
    startX = 477
    startY = 652
    endX = 606
    endY = 688
  if number == 4:
    # model = load_model(config_name.MODEL_PATH)
    name = "Name.jpg"
    startX = 471
    startY = 715
    endX = 621
    endY = 748
  if number == 5:
    # model = load_model(config_fiOtch.MODEL_PATH)
    name = "Secend_name.jpg"
    startX = 472
    startY = 752
    endX = 621
    endY = 781
  if number == 6:
    # model = load_model(config_d_r.MODEL_PATH)
    name = "DR.jpg"
    startX = 528
    startY = 786
    endX = 670
    endY = 814

  # # image = cv2.imread("example.jpg")
  image = arr2[0]
  # image3 = image

  # # # show the output image
  # # cv2.imshow("Output", image)
  # # cv2.waitKey(0)

  # image = cv2.resize(image, (224, 224))

  # # load our trained bounding box regressor from disk
  # # print("[INFO] loading object detector...")
  # # model = load_model(config.MODEL_PATH)

  # # loop over the images that we'll be testing using our bounding box
  # # regression model
  # # for imagePath in imagePaths:

  #   # load the input image (in Keras format) from disk and preprocess
  #   # it, scaling the pixel intensities to the range [0, 1]
  # image = img_to_array(image) / 255.0
  # image = np.expand_dims(image, axis=0)

  #   # make bounding box predictions on the input image
  # preds = model.predict(image)[0]
  # (startX, startY, endX, endY) = preds

  #   # load the input image (in OpenCV format), resize it such that it
  #   # fits on our screen, and grab its dimensions
  # image = image3
  # image = imutils.resize(image, width=600)
  # (h, w) = image.shape[:2]

  #   # scale the predicted bounding box coordinates based on the image
    # dimensions
  # startX = int(startX * w)
  # startY = int(startY * h)
  # endX = int(endX * w)
  # endY = int(endY * h)

    # draw the predicted bounding box on the image
  cv2.rectangle(image, (startX, startY), (endX, endY), (0, 255, 0), 2)

  cropped = image[startY:endY, startX:endX]
  if number == 2:
      cv2.imwrite('app/static/images/scan/SN_crop.jpg', cropped)
      im = Image.open('app/static/images/scan/SN_crop.jpg')
      im_rotate = im.rotate(90, expand=True)
      im_rotate.save('app/static/images/scan/SN_crop.jpg', quality=95)
      im.close()
      cropped=cv2.imread('app/static/images/scan/SN_crop.jpg')
  #   im = Image.open('SN.jpg')

  #   # show the output image
  cv2.imwrite(imagePath + name, image)
  arr2[0] = image
  # cv2.waitKey(0)
  if number != 1 :
    arr2[1] = tesseract(cropped)
  # return(cropped)
  

  return(arr2)