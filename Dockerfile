# прудварительно создаем requirement.txt используя скрипт ./script/req_create.sh
# прикручиваем образ
FROM python:3
# устанвливаем значение переменной, для того чтобы выходные данные отправлялись в буфер 
# для того чтобы видеть журналы в режиме реального времени
WORKDIR /usr/src/app
ENV PYTHONUNBUFFERED = 1
ENV PYTHONDONTWRITEBYTECODE = 1
# копируем requirement.txt в образ
COPY requirement.txt .
#устанавливает необходимые зависимости поверх образа python:3.8.10-slim-buster 
RUN apt-get update -y
# доустановить вещи ниже без них не работает
RUN apt-get install ffmpeg libsm6 libxext6 gcc tesseract-ocr libtesseract-dev libleptonica-dev tesseract-ocr-rus -y

# RUN build-essential cmake unzip pkg-config gcc-6 g++-6 screen libxmu-dev libxi-dev libglu1-mesa libglu1-mesa-dev libjpeg-dev libpng-dev libtiff-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev libopenblas-dev libatlas-base-dev liblapack-dev gfortran libhdf5-serial-dev python3-dev python3-tk python-imaging-tk libgtk-3-dev -y

RUN pip install -r requirement.txt
# Копируем репозиторий в образ
COPY . .
# EXPOSE 8888
# docker-compose build 
# docker-compose up -d 
